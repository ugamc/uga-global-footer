# University of Georgia Global Footer

![UGA footer](./screenshot.png)

## What’s Included?

* `src` directory — uncompiled source code
* `dist` directory — compiled code ready for use in production
* `screenshot.jpg` — intended display of footer
* build scripts and dependencies

## Guidelines

### Required Links

#### Privacy Policy

A link to the EITS [Privacy](https://eits.uga.edu/access_and_security/infosec/pols_regs/policies/privacy/) Policy is required on all websites.

#### Student Complaints

Units categorized as a [functional area](https://studentaffairs.uga.edu/site/content_page/functional-areas) are required to link to the Student Complaint Portal in compliance with the [Procedures for Written Student Complaints policy](https://provost.uga.edu/policies/academic-affairs-policy-manual/4-05-student-appeals/#p-4-05-5).

These [functional areas](https://studentaffairs.uga.edu/site/content_page/functional-areas) may choose to include this link in this footer or elsewhere on their website.

#### Human Trafficking Notice

According to Georgia Code Section 16-5-47, a link to the Georgia Bureau of Investigation Human Trafficking Notice is required the homepage of government entities. This link is only required on the [university homepage](https://www.uga.edu/).

#### Ethics Reporting Hotline

[USG Policy 16.5.8](https://www.usg.edu/business_procedures_manual/section16/C2343) requires the [UGA Hotline Reporting System](https://legal.uga.edu/guidance/compliance-ethics-reporting-hotline) be linked on the University’s homepage or other prominent location accessible by employees, students, and the public. This link is only required on the [university homepage](https://www.uga.edu/).

### Additional Text Links

Individual sites can append up to three additional links the footer. These should retain the existing format.

### Social Media Icons and Links

These icons should link to the official University of Georgia accounts. Site-specific social media links can be included in a separate site-specific footer or elsewhere on the site, such as the header.

## Installation Instructions

1. Upload contents of `dist/_assets` to your server. Make note of upload location if uploading to a different directory.
2. Add HTML code from `dist/index.html` to your page or template.
	* Contents of the `<body />` tag are the entire footer. Paste at the end of your HTML document before the closing `</body>` tag.
    * Add up to three additional text links that are appropriate for your site.
	* Include the stylesheet `<link />` tag within the `<head>…</head>`.
    * Include the JavaScript `<script />` tag at the end of the `<body>…</body>`
	* Update your paths to match your upload location.
3. Compare display of the footer on your site to `screenshot.jpg`. If significant differences exist between the example and your site, adjust CSS accordingly. If your responsive breakpoints differ from the footer, you may adjust the CSS container widths and breakpoints to match your website.

## Developer Notes

The `dist` directory contains production-ready code. If you need to make modifications to the source code and create your own build, here are some instructions.

### Prerequisites

- [Node.js/NPM](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/lang/en/docs/install/)
- [Gulp](http://gulpjs.com/)

### Installing

After installing prerequisites, install the app dependencies.

```
yarn install
```

Start up Gulp!

```
gulp
```

The task runner will build pages, CSS, and JS and start a server. The site will automatically open in a browser to preview.

### Deployment

Run Gulp with a flag to produce production-ready code

```
gulp build --production
```
